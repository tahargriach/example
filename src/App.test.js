import { render, screen } from '@testing-library/react';
import App from './App';

test('renders the correct text', () => {
  render(<App />);
  const textElement = screen.getByText(/sss/i); // Update this line to match the actual text
  expect(textElement).toBeInTheDocument();
});